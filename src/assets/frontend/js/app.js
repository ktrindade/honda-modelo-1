'use strict';
window.$ = window.jQuery = require('jquery');
window.boostrap = require('bootstrap-sass');
window.slick = require('slick-carousel');
window.ekkoLightbox = require('ekko-lightbox')

$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

$('#new-car-tabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
});

$('#tabs-home a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
});

$('.slider-carros-home').slick({
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 5
            }
            },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 3
            }
            },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
            }
        ]
});

$('.slider-another-models').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 5
            }
            },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 3
            }
            },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
            }
        ]
});

$('.slider-galeria').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3
            }
            },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 3
            }
            },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
            }
        ]
});
